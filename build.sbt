import com.typesafe.sbt.packager.docker.{DockerApiVersion, DockerVersion}
import Syntax._

val circeVersion = "0.14.2"

lazy val TheBot = (project in file("."))
  .enablePlugins(JavaAppPackaging, AshScriptPlugin)
  .settings(
    name             := "gitlab-merge-requests-linking-bot",
    idePackagePrefix := Some("io.gitlab.mateuszjaje"),
    scalaVersion     := "3.3.1",
    version          := "1.0-SNAPSHOT",
    libraryDependencies ++= Seq(
      "io.gitlab.mateuszjaje" %% "jira4s-sttp-zio"    % "2.5.1",
      "io.gitlab.mateuszjaje" %% "gitlab4s-sttp-zio"  % "2.7.33",
      "org.mongodb.scala"     %% "mongo-scala-driver" % "4.7.1" cross CrossVersion.for3Use2_13,
      "ch.qos.logback"         % "logback-classic"    % "1.5.2",
      "com.github.scopt"      %% "scopt"              % "4.+",
      "com.cronutils"          % "cron-utils"         % "9.1.6",
      "org.codehaus.janino"    % "janino"             % "3.1.12",
      "com.github.pureconfig" %% "pureconfig-core"    % "0.17.8",
      "dev.zio"               %% "zio-logging-slf4j"  % "2.+",
    ),
    dockerBaseImage  := sys.env.getOrElse("DOCKER_BASE_IMAGE", "openjdk:11"),
    dockerVersion    := Some(DockerVersion(20, 10, 17, Some("hardcoded"))),
    dockerApiVersion := Some(DockerApiVersion(1, 40)),
  )
