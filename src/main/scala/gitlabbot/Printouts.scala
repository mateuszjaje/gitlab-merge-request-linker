package io.gitlab.mateuszjaje
package gitlabbot

import gitlabbot.logic.{DescriptionUpdated, UpdateMRDescriptionResult}
import jiraclient.models.IssueKey

import zio.Console.printLine
import zio.ZIO

import java.io.IOException

object Printouts:
  def printOutUpdatedMrs(data: Map[IssueKey, Vector[UpdateMRDescriptionResult]]): ZIO[Any, IOException, Any] = {
    val nonEmptyData = data.view.mapValues(_.collect { case e: DescriptionUpdated => e }).filter(_._2.nonEmpty)

    if (nonEmptyData.nonEmpty) {
      ZIO.foreach(
        nonEmptyData
          .map { case (issue, updates) =>
            s"${issue.toString}: ${updates.length} mrs updated. One of them: ${updates.head.mr._mr.web_url}"
          },
      )(printLine(_))
    } else {
      printLine("No open issues!")
    }
  }
