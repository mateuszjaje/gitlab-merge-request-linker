package io.gitlab.mateuszjaje
package gitlabbot

import gitlabbot.shared.MgthError

import scopt.OParser
import zio.{IO, ZIO}

import scala.concurrent.duration.{Duration, DurationInt, FiniteDuration}

case class CliArguments(
    interval: Duration = 5.minutes,
    duration: Duration = 1.minutes, // effectively once
)

object CliArguments:
  val builder = OParser.builder[CliArguments]
  val parser = {
    import builder.*
    OParser.sequence(
      head("this bot", "1.0.0"),
      opt[Duration]("stop-after")
        .action((x, c) => c.copy(duration = x))
        .text("duration how long should it work"),
      opt[Duration]("interval")
        .action((x, c) => c.copy(interval = x))
        .text("interval"),
    )
  }

  def parse(args: Seq[String]): IO[MgthError, CliArguments] =
    ZIO.fromOption(OParser.parse(parser, args, CliArguments())).orElseFail(MgthError.invalidData("can't parse command line args"))
