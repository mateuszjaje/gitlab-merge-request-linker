package io.gitlab.mateuszjaje
package gitlabbot.logic

import gitlabbot.shared.{BranchInfo, JiraIssueSimple, MrInfo}
import jiraclient.models.IssueKey

import scala.collection.mutable

case class GitlabMRDescriptionRenderingConfig(
    epicMdEntry: Option[String],
    ticketMdEntry: Option[String],
    descriptionSeparator: String,
)

object RichStringBuilder {
  implicit class RichSB(sb: StringBuilder):
    def appendJiraLink(key: IssueKey, link: String): StringBuilder =
      sb.append("[**\\[")
        .append(key.value.toUpperCase)
        .append("\\]**](")
        .append(link)
        .append(")")

}

trait GitlabMRDescriptionRendering {
  import RichStringBuilder.RichSB

  def renderDescriptionOfMergeRequest(
      pr: MrInfo,
      issue: JiraIssueSimple,
      epic: Option[JiraIssueSimple],
      allPrsInIssue: List[MrInfo],
      freeBranches: Vector[BranchInfo],
      config: GitlabMRDescriptionRenderingConfig,
  ): String = {
    val companionPRs = allPrsInIssue.filterNot(x => x.uniqueId == pr.uniqueId).sortBy(x => (x.projectName, x.id))
    val parts        = pr.description.split(config.descriptionSeparator)

    val renderedPart = new mutable.StringBuilder()
    if (!issue.isIgnoreMe) {
      renderedPart.append("  \nLinks:  \n  \n")
      epic.foreach { definedEpic =>
        renderedPart
          .append(config.epicMdEntry.map(_ + " ").getOrElse(""))
          .appendJiraLink(definedEpic.key, definedEpic.webUrl)
          .append(": ")
          .append(definedEpic.title)
          .append("  \n")
      }
      renderedPart
        .append(config.ticketMdEntry.map(_ + " ").getOrElse(""))
        .appendJiraLink(issue.key, issue.webUrl)
        .append(": ")
        .append(issue.title)
        .append("  \n")
    }
    if (companionPRs.nonEmpty || freeBranches.nonEmpty) renderedPart.append("  \nCompanion MRs:  \n  \n")
    companionPRs.foreach { companionPr =>
      val repoName = companionPr.projectName
      renderedPart.append("* [")
      if (!companionPr.open) renderedPart.append("~~")
      renderedPart.append("**#").append(companionPr.id).append("** in the **").append(repoName).append("**")
      if (!companionPr.open) renderedPart.append("~~")
      renderedPart.append("](").append(companionPr.webUrl).append(")")
      renderedPart.append("  \n")
    }
    freeBranches.foreach { branch =>
      renderedPart.append("* [")
      renderedPart.append("**").append(branch.name).append("** in the **").append(branch.projectName).append("**")
      renderedPart.append("](").append(branch.webUrl).append(")")
      renderedPart.append("  \n")
    }
    renderedPart.append("  \n")

    val result = new mutable.StringBuilder()
    result.append(parts.head.trim).append("  \n  \n")
    result.append(config.descriptionSeparator).append("  \n")
    result.append(renderedPart)

    result.result()
  }

}
