package io.gitlab.mateuszjaje
package gitlabbot.logic

import gitlabbot.shared.*
import gitlabclient.models.MergeRequestInfo
import gitlabclient.models.MergeRequestStates.Open
import jiraclient.JiraRestAPIV2
import jiraclient.models.{Icon, IssueKey, JiraRemoteLink}

import com.typesafe.scalalogging.LazyLogging
import zio.{IO, UIO, ZIO}

trait JiraLinksUpdate extends LazyLogging with BranchIdHelpers with JiraAPIHelpers {
  def jira: JiraRestAPIV2[UIO]

  private val gitlabIcon                         = Icon(Some("https://gitlab.com/favicon.ico"), Some("Gitlab"), None)
  private val defaultBranches                    = Set("develop", "master", "main")
  implicit val IssueOrdering: Ordering[IssueKey] = Ordering.by((x: IssueKey) => x.value)

  protected def updateJiraTicketLinks(
      issue: IssueKey,
      prs: Vector[MergeRequestInfo],
      branches: Vector[BranchInfo],
      repoNames: Map[BigInt, ProjectSimpleInfo],
  ): IO[MgthError, Unit] = {

    def updateMrLink(currentOnes: Vector[JiraRemoteLink])(mr: MergeRequestInfo) = {
      val repoName         = repoNames(mr.target_project_id).humanName
      val targetBranchInfo = Some(mr.target_branch).filterNot(defaultBranches.contains).map(b => s"(to $b)").getOrElse("")
      val linkName         = s"!${mr.iid} MR in the $repoName $targetBranchInfo".trim
      val linkId           = mergeRequestUniqueId(mr)
      val linkResolved     = mr.state != Open

      val needsUpdate = currentOnes
        .find(_.globalId.contains(linkId))
        .map { currentOne =>
          val nameIsTheSame     = currentOne.`object`.title == linkName
          val resolvedIsTheSame = currentOne.`object`.status.flatMap(_.resolved).contains(linkResolved)
          val urlIsTheSame      = currentOne.`object`.url == mr.web_url

          val isTheSame = nameIsTheSame && resolvedIsTheSame && urlIsTheSame

          if (!nameIsTheSame)
            logger.info(s"JiraLink $linkId in $issue will be updated because of title change: ${currentOne.`object`.title} -> $linkName")
          if (!resolvedIsTheSame)
            logger.info(
              s"JiraLink $linkId in $issue will be updated because of resolved change: ${currentOne.`object`.status.flatMap(_.resolved)} -> $linkResolved",
            )
          if (!urlIsTheSame)
            logger.info(s"JiraLink $linkId in $issue will be updated because of url change: ${currentOne.`object`.url} -> ${mr.web_url}")

          !isTheSame
        }
        .getOrElse {
          logger.info(s"JiraLink $linkId in $issue will be created")
          true
        }

      if (needsUpdate) {
        createOrUpdateIssueLink(issue, linkId, mr.web_url, linkName, linkResolved, gitlabIcon)
          .as(LinkUpdateSucc)
          .catchSome { case CausedByJira(_, jiraclient.HttpError(403, _, _, _, _), _) =>
            ZIO.succeed(LinkUpdateForbidden(issue))
          }
      } else ZIO.succeed(LinkUpdateSkipped: LinkUpdateResult)

    }

    def updateBranchLink(branch: BranchInfo) = {
      val linkName = s"Feature branch in the ${branch.projectName}: ${branch.name}"
      createOrUpdateIssueLink(issue, branchUniqueId(branch), branch.webUrl, linkName, resolved = false, gitlabIcon)
        .as(LinkUpdateSucc)
        .catchSome { case CausedByJira(_, jiraclient.HttpError(403, _, _, _, _), _) =>
          ZIO.succeed(LinkUpdateForbidden(issue))
        }
    }

    val branchesWithMrs = prs.map(branchUniqueId).toSet
    val freeBranches    = branches.filterNot(branch => branchesWithMrs.contains(branchUniqueId(branch)))
    for {
      currentLinks <- getIssueLinks(issue)
      mrLinks      <- ZIO.foreach(prs)(updateMrLink(currentLinks))
      branchLinks  <- ZIO.foreach(freeBranches)(updateBranchLink)
      problematicIssues = (branchLinks ++ mrLinks).collect { case LinkUpdateForbidden(i) => i }.distinct.sorted
      _                 = if (problematicIssues.nonEmpty) logger.info(s"$problematicIssues can't be updated due to forbidden")
    } yield ()
  }

  private def getRedundantLinks(currentLinks: Vector[JiraRemoteLink]): Vector[JiraRemoteLink] = {
    def filterRedundantLinks(current: Vector[JiraRemoteLink]) = {
      val goodOne = current
        .filter(_.globalId.exists(_.matches("""^((mr)|b):\d+:\d+$""")))
        .filter(_.relationship.isEmpty)
        .filter(_.`object`.title.contains("MR in the"))
      if (goodOne.length == 1) {
        current.diff(List(goodOne.head))
      } else {
        Vector.empty
      }
    }
    currentLinks.groupBy(x => x.`object`.url).view.mapValues(filterRedundantLinks).values.toVector.flatten
  }

  protected def dropRedundantLinks(ticket: IssueKey): IO[MgthError, Unit] = {
    for {
      currentLinks <- getIssueLinks(ticket)
      linksToDrop = getRedundantLinks(currentLinks).map(_.id.get)
      _ <- ZIO.foreach(linksToDrop)(deleteJiraRemoteLink(ticket, _))
    } yield ()
  }

  protected def collectRedundantLinks(ticket: IssueKey): ZIO[Any, MgthError, Vector[Int]] =
    getIssueLinks(ticket)
      .map(currentLinks => getRedundantLinks(currentLinks).map(_.id.get))

  protected def dropSingleRedundantLink(ticket: IssueKey): IO[MgthError, Unit] = {
    for {
      linksToDrop <- collectRedundantLinks(ticket).map(_.take(1))
      _           <- ZIO.foreach(linksToDrop)(deleteJiraRemoteLink(ticket, _))
    } yield ()
  }

  protected val ignoreNonExistingIssue: PartialFunction[MgthError, IO[MgthError, Unit]] = { //@formatter:off
    case CausedByJira(_, jiraclient.HttpError(403, "http-response-error", _, _, Some("""{"errorMessages":["You do not have the permission to see the specified issue."],"errors":{}}""")), _) =>
      ZIO.unit
    case CausedByJira(_, jiraclient.HttpError(404, "http-response-error", _, "get-issue-remote-links", Some("""{"errorMessages":["Issue Does Not Exist"],"errors":{}}""")), _) =>
      ZIO.unit
  } //@formatter:on

}

trait LinkUpdateResult

case object LinkUpdateSucc    extends LinkUpdateResult
case object LinkUpdateSkipped extends LinkUpdateResult

case class LinkUpdateForbidden(issue: IssueKey) extends LinkUpdateResult
