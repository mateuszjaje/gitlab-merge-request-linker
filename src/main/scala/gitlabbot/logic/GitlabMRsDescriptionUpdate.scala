package io.gitlab.mateuszjaje
package gitlabbot.logic

import gitlabbot.ImageLinksConfig
import gitlabbot.logic.{GitlabMRDescriptionRendering, GitlabMRDescriptionRenderingConfig}
import gitlabbot.shared.*
import gitlabclient.*
import gitlabclient.models.{MergeRequestFull, UpdateMRPayload}
import jiraclient.models.IssueKey

import com.typesafe.scalalogging.LazyLogging
import zio.{IO, UIO, ZIO}

import scala.concurrent.duration.*

trait GitlabMRsDescriptionUpdate extends LazyLogging with GitlabMRDescriptionRendering {
  def gitlab: GitlabRestAPIV2[UIO]

  protected def updateDescriptionsOfMRs(
      issue: JiraIssueSimple,
      epic: Option[JiraIssueSimple],
      mrs: Vector[MrInfo],
      freeBranches: Vector[BranchInfo],
      config: GitlabLinkingMRsConfig,
  ): IO[MgthError, Vector[UpdateMRDescriptionResult]] = {

    val renderingConfig = config.renderingConfig
    val allMrs          = mrs.map(mr => mr.uniqueId -> mr).toMap

    def updateMR(mr: MrInfo): IO[MgthError, UpdateMRDescriptionResult] = {
      val mrR                = allMrs(mr.uniqueId)
      val updatedDescription = renderDescriptionOfMergeRequest(mrR, issue, epic, allMrs.values.toList, freeBranches, renderingConfig)
      if (updatedDescription.trim != mr.description.trim) {
        updateMRDescriptionWithRetries(mr, updatedDescription)
          .as(UpdateMRDescriptionResult.updated(mr, issue.key, mr.description, updatedDescription))
      } else {
        ZIO.succeed(UpdateMRDescriptionResult.noUpdate)
      }
    }

    ZIO.foreach(mrs)(updateMR)
  }

  private def updateMRDescriptionWithRetries(mr: MrInfo, updatedDescription: String) = {
    def executeAction = gitlab.updateMergeRequest(mr.projectId, mr.id, UpdateMRPayload.description(updatedDescription)).absolve

    def retryableUpdate(retriesLeft: Int): IO[GitlabError, MergeRequestFull] = {
      executeAction.catchSome {
        case GitlabHttpError(500, _, _, _, Some("""{"message":"500 Internal Server Error"}""")) if retriesLeft > 0 =>
          logger.warn(s"Retrying MR update, retries left $retriesLeft")
          retryableUpdate(retriesLeft - 1)
        case GitlabRequestingError("try-http-backend-left", _, _: sttp.client3.SttpClientException.ReadException) if retriesLeft > 0 =>
          logger.warn(s"Retrying MR update 2, retries left $retriesLeft")
          retryableUpdate(retriesLeft - 1)
      }
    }

    retryableUpdate(5).mapError(MgthError(_))
  }

}

sealed trait UpdateMRDescriptionResult extends Product with Serializable

object UpdateMRDescriptionResult {
  def noUpdate: UpdateMRDescriptionResult = NoUpdateBecauseNoChange

  def dontUpdate: UpdateMRDescriptionResult = DontUpdateBecauseProjectOutOfScope

  def cannotUpdate: UpdateMRDescriptionResult = CannotUpdateBecauseForbidden

  def updated(mr: MrInfo, ticket: IssueKey, old: String, current: String): UpdateMRDescriptionResult =
    DescriptionUpdated(mr, ticket, old, current)

}

case object NoUpdateBecauseNoChange extends UpdateMRDescriptionResult

case object DontUpdateBecauseProjectOutOfScope extends UpdateMRDescriptionResult

case object CannotUpdateBecauseForbidden extends UpdateMRDescriptionResult

case class DescriptionUpdated(mr: MrInfo, ticket: IssueKey, old: String, current: String) extends UpdateMRDescriptionResult
