package io.gitlab.mateuszjaje
package gitlabbot.logic

import gitlabbot.logic.{GitlabMRsDescriptionUpdate, JiraLinksUpdate, UpdateMRDescriptionResult}
import gitlabbot.shared.*
import gitlabbot.shared.MyZIOSyntax.rightE
import gitlabbot.storage.mongo.JiraIssueSimpleDAO
import gitlabbot.storage.{JiraSimpleIssueCache, ProjectSimpleInfoCache}
import gitlabbot.{ImageLinksConfig, MonitoredProjects}
import gitlabclient.GitlabRestAPIV2
import gitlabclient.models.{MergeRequestInfo, MergeRequestState, MergeRequestStates}
import jiraclient.JiraRestAPIV2
import jiraclient.models.IssueKey

import com.typesafe.scalalogging.LazyLogging
import zio.{IO, UIO, URLayer, ZIO, ZLayer}

import java.time.ZonedDateTime

object UpdateLinksInMRsWorker:
  val live: URLayer[
    GitlabRestAPIV2[UIO] & JiraRestAPIV2[UIO] & JiraSimpleIssueCache & ProjectSimpleInfoCache & GitlabLinkingMRsConfig & MonitoredProjects,
    UpdateLinksInMRsWorker,
  ] =
    ZLayer.fromFunction(new UpdateLinksInMRsWorker(_, _, _, _, _, _))

class UpdateLinksInMRsWorker(
    val gitlab: GitlabRestAPIV2[UIO],
    val jira: JiraRestAPIV2[UIO],
    jiraIssueCache: JiraSimpleIssueCache,
    val projectNameCache: ProjectSimpleInfoCache,
    val imageLinksConfig: GitlabLinkingMRsConfig,
    monitoredGroups: MonitoredProjects,
) extends LazyLogging
    with GitlabAPIHelpers
    with GitlabMRsDescriptionUpdate
    with BranchIdHelpers
    with JiraLinksUpdate {

  def linkMrsForIssues(jiraIssues: Set[IssueKey]): IO[MgthError, Map[IssueKey, Vector[UpdateMRDescriptionResult]]] = {
    for {
      // @formatter:off
      foundIssues <- ZIO.foreach(jiraIssues.toVector) { c =>
        jiraIssueCache.getIssueSimple(jira, c).map(i => Vector(i.key -> i, c -> i))
          .catchSome {
            case CausedByJira(_, jiraclient.HttpError(403, "http-response-error", _, _, Some("""{"errorMessages":["You do not have the permission to see the specified issue."],"errors":{}}""")), _) =>
              ZIO.succeed(Vector.empty)
            case CausedByJira(_, jiraclient.HttpError(404, "http-response-error", _, _, Some("""{"errorMessages":["Issue does not exist or you do not have permission to see it."],"errors":{}}""")), _) =>
              ZIO.succeed(Vector.empty)
          }
        } // @formatter:on
        .map(_.flatten)
        .map(_.toMap)
      mrs <- ZIO
        .foreach(foundIssues.keys.toVector)(issue => findAllMRsWithIssueKeyInName(monitoredGroups, issue).map(issue -> _))
        .map(_.toMap)
      allBranchesInAllProjects <- loadAllBranchesInKnownProjects(imageLinksConfig.projectsToCheckForBranches)
      projectNames             <- resolveProjectNames(mrs.values)
      epics <- ZIO
        .foreach(foundIssues.values.flatMap(_.epicKey))(c => jiraIssueCache.getIssueSimple(jira, c).map(i => Vector(i.key -> i, c -> i)))
        .map(_.flatten)
      allUpdates <- ZIO.foreach(mrs.toVector)(handle(projectNames, allBranchesInAllProjects, foundIssues ++ epics))
    } yield allUpdates.toMap
  }

  private def handle(projectNames: Map[BigInt, ProjectSimpleInfo], allBranches: Vector[BranchInfo], issues: Map[IssueKey, JiraIssueSimple])(
      data: (IssueKey, Vector[MergeRequestInfo]),
  ) = {
    val value = data._2.map(x => MrInfo(x, projectNames))

    val branches           = allBranches.filter(_.name.toLowerCase.contains(data._1.value.toLowerCase))
    val branchesWithoutMRs = findFreeBranches(data._2, branches)

    for {
      updatedMrs <- updateDescriptionsOfMRs(
        issues(data._1),
        issues(data._1).epicKey.flatMap(issues.get),
        value,
        branchesWithoutMRs,
        imageLinksConfig,
      )
      _ <- updateJiraTicketLinks(data._1, data._2, branches, projectNames)
        .catchSome(ignoreNonExistingIssue)
        .unless(issues(data._1).isIgnoreMe)
    } yield data._1 -> updatedMrs
  }

  def collectMrs(state: MergeRequestState, lastUpdate: Option[ZonedDateTime]): IO[MgthError, Set[IssueKey]] = {
    for {
      foundMrs <- ZIO
        .foreach(monitoredGroups.groups) { groupId =>
          ZIO.debug(s"collectMrs.foundMrs.gitlab.getGroupMergeRequests($groupId, updatedAfter=$lastUpdate, state=$state)") *>
            gitlab
              .getGroupMergeRequests(groupId, updatedAfter = lastUpdate, state = state)
              .absolve
              .mapError(MgthError(_))
        }
        .map(_.flatten)
      extraProjectMrs <- ZIO
        .foreach(monitoredGroups.extraProjects) { projId =>
          ZIO.debug(s"collectMrs.extraProjectMrs.gitlab.getMergeRequests($projId, updatedAfter=$lastUpdate, state=$state)") *>
            gitlab
              .getMergeRequests(projId, updatedAfter = lastUpdate.orNull, state = state)
              .absolve
              .mapError(MgthError(_))
        }
        .map(_.flatten)
      allMrs     = foundMrs ++ extraProjectMrs
      allTickets = allMrs.flatMap(IssueFinder.findIssueIn).toSet
      existingTickets <- ZIO
        .foreach(allTickets) { key =>
          jiraIssueCache.getIssueSimple(jira, key).map(Some(_)).catchSome { //@formatter:off
            case CausedByJira(_, jiraclient.HttpError(403, "http-response-error", _, _, Some("""{"errorMessages":["You do not have the permission to see the specified issue."],"errors":{}}""")), _) =>
              ZIO.none
            case CausedByJira(_, jiraclient.HttpError(404, "http-response-error", _, _, Some("""{"errorMessages":["Issue does not exist or you do not have permission to see it."],"errors":{}}""")), _) =>
              ZIO.none
          } //@formatter:on
        }
        .map(_.flatten.map(_.key).toSet)
    } yield existingTickets
  }

  def linkAllOpenedAndRecentlyMergedMrs(
      lastUpdate: Option[ZonedDateTime],
  ): IO[MgthError, Map[IssueKey, Vector[UpdateMRDescriptionResult]]] = {
    for {
      openIssues <- collectMrs(MergeRequestStates.Open, lastUpdate)
      _ = logger.info(s"Found open issues to link: $openIssues")
      recentlyMerged <- collectMrs(MergeRequestStates.Merged, lastUpdate)
      _      = logger.info(s"Found issues with recently merged mrs: $recentlyMerged")
      issues = openIssues ++ recentlyMerged

      result <- issues.grouped(20).foldLeft(ZIO.rightE[MgthError](Map.empty[IssueKey, Vector[UpdateMRDescriptionResult]])) {
        case (acc, batch) =>
          for {
            a    <- acc
            next <- linkMrsForIssues(batch)
          } yield a ++ next
      }
    } yield result
  }

}

case class GitlabLinkingMRsConfig(
    renderingConfig: GitlabMRDescriptionRenderingConfig,
    projectsToCheckForBranches: Set[BigInt],
)
