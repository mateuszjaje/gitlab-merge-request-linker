package io.gitlab.mateuszjaje
package gitlabbot.storage

import gitlabbot.shared.BranchToKeep

import zio.Task

import java.time.ZonedDateTime

trait SettingsStore:
  def storeLastUpdated(date: ZonedDateTime): Task[Unit]
  def loadLastChecked: Task[Option[ZonedDateTime]]
  def readBranchesToKeep: Task[List[BranchToKeep]]
