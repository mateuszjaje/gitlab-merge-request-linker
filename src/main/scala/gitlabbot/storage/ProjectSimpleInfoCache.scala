package io.gitlab.mateuszjaje
package gitlabbot.storage

import gitlabbot.shared.{MgthError, ProjectSimpleInfo}

import zio.IO

trait ProjectSimpleInfoCache:
  def store(repoId: BigInt, projectName: String, projectWebUrl: String): IO[MgthError, Unit]
  def getProjectSimpleInfo(ids: Vector[BigInt]): IO[MgthError, Map[BigInt, ProjectSimpleInfo]]
