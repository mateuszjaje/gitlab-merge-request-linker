package io.gitlab.mateuszjaje
package gitlabbot.storage.mongo.codecs

import org.bson.codecs.configuration.CodecConfigurationException
import org.bson.codecs.{Codec, DecoderContext, EncoderContext}
import org.bson.{BsonReader, BsonType, BsonWriter}

object BigIntCodec extends Codec[BigInt]:
  override def decode(reader: BsonReader, decoderContext: DecoderContext): BigInt = {
    reader.getCurrentBsonType match {
      case BsonType.STRING =>
        BigInt(reader.readString)
      case t =>
        throw new CodecConfigurationException(s"Could not decode into BigInt, expected STRING BsonType but got '$t'.")
    }
  }

  override def encode(writer: BsonWriter, value: BigInt, encoderContext: EncoderContext): Unit = {
    writer.writeString(value.toString())
  }

  override val getEncoderClass: Class[BigInt] = classOf[BigInt]
