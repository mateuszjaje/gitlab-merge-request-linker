package io.gitlab.mateuszjaje
package gitlabbot.storage.mongo.codecs

import jiraclient.models.IssueKey

import org.bson.codecs.configuration.CodecConfigurationException
import org.bson.codecs.{Codec, DecoderContext, EncoderContext}
import org.bson.{BsonReader, BsonType, BsonWriter}

import java.time.{Instant, ZoneOffset, ZonedDateTime}
import java.util.UUID
import scala.reflect.ClassTag

object UTCZonedDateTimeMongoCodec extends Codec[ZonedDateTime]:
  override def decode(reader: BsonReader, decoderContext: DecoderContext): ZonedDateTime = {
    reader.getCurrentBsonType match {
      case BsonType.DATE_TIME =>
        Instant.ofEpochMilli(reader.readDateTime).atZone(ZoneOffset.UTC)
      case t =>
        throw new CodecConfigurationException(s"Could not decode into ZonedDateTime, expected DATE_TIME BsonType but got '$t'.")
    }
  }

  override def encode(writer: BsonWriter, value: ZonedDateTime, encoderContext: EncoderContext): Unit = {
    try
      writer.writeDateTime(value.withZoneSameInstant(ZoneOffset.UTC).toInstant.toEpochMilli)
    catch {
      case e: ArithmeticException =>
        throw new CodecConfigurationException(
          s"Unsupported LocalDateTime value '$value' could not be converted to milliseconds: ${e.getMessage}",
          e,
        )
    }
  }

  override val getEncoderClass: Class[ZonedDateTime] = classOf[ZonedDateTime]
