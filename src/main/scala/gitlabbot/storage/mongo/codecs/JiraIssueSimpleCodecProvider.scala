package io.gitlab.mateuszjaje
package gitlabbot.storage.mongo.codecs

import gitlabbot.shared.JiraIssueSimple
import jiraclient.models.IssueKey

import org.bson.codecs.configuration.{CodecProvider, CodecRegistry}
import org.bson.codecs.{Codec, EncoderContext}
import org.bson.{BsonInvalidOperationException, BsonWriter}

object JiraIssueSimpleCodecProvider extends CodecProvider {
  val JiraIssueSimpleClass = classOf[JiraIssueSimple]

  override def get[T](clazz: Class[T], registry: CodecRegistry) =
    if (JiraIssueSimpleClass.isAssignableFrom(clazz)) JiraIssueSimpleCodec(registry).asInstanceOf[Codec[T]] else null

  final case class JiraIssueSimpleCodec(reg: CodecRegistry)
      extends SemiMacroCodec[JiraIssueSimple](
        JiraIssueSimpleClass,
        reg,
        Map(
          "key"         -> classOf[IssueKey],
          "webUrl"      -> classOf[String],
          "title"       -> classOf[String],
          "epicKey"     -> classOf[IssueKey],
          "description" -> classOf[String],
        ),
      ) {
    override protected def encodeCaseClass(writer: BsonWriter, value: JiraIssueSimple, encoderContext: EncoderContext): Unit = {
      writeFieldValue(writer, "key", value.key, encoderContext)
      writeFieldValue(writer, "webUrl", value.webUrl, encoderContext)
      writeFieldValue(writer, "title", value.title, encoderContext)
      value.epicKey.foreach(writeFieldValue(writer, "epicKey", _, encoderContext))
      writeFieldValue(writer, "description", value.description, encoderContext)
    }

    override protected def construct(fieldsData: Map[String, Any]) = {
      JiraIssueSimple(
        key = fieldsData.getOrElse("key", throw new BsonInvalidOperationException("Missing field: \"key\"")).asInstanceOf[IssueKey],
        webUrl = fieldsData.getOrElse("webUrl", throw new BsonInvalidOperationException("Missing field: \"webUrl\"")).asInstanceOf[String],
        title = fieldsData.getOrElse("title", throw new BsonInvalidOperationException("Missing field: \"title\"")).asInstanceOf[String],
        epicKey = fieldsData.get("epicKey").asInstanceOf[Option[IssueKey]],
        description = fieldsData
          .getOrElse("description", throw new BsonInvalidOperationException("Missing field: \"description\""))
          .asInstanceOf[String],
      )
    }

  }

}
