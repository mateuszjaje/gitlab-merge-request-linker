package io.gitlab.mateuszjaje
package gitlabbot.storage.mongo.codecs

import gitlabbot.shared.JiraIssueSimple
import gitlabbot.storage.mongo.JiraIssueSimpleDAO
import jiraclient.models.IssueKey

import org.bson.codecs.configuration.{CodecProvider, CodecRegistry}
import org.bson.codecs.{Codec, EncoderContext}
import org.bson.{BsonInvalidOperationException, BsonWriter}

import java.time.ZonedDateTime

object JiraIssueSimpleDAOCodecProvider extends CodecProvider {
  val JiraIssueSimpleDAOClass = classOf[JiraIssueSimpleDAO]

  override def get[T](clazz: Class[T], registry: CodecRegistry) =
    if (JiraIssueSimpleDAOClass.isAssignableFrom(clazz)) JiraIssueSimpleDAOCodec(registry).asInstanceOf[Codec[T]] else null

  final case class JiraIssueSimpleDAOCodec(reg: CodecRegistry)
      extends SemiMacroCodec[JiraIssueSimpleDAO](
        JiraIssueSimpleDAOClass,
        reg,
        Map(
          "key"       -> classOf[IssueKey],
          "createdAt" -> classOf[ZonedDateTime],
          "data"      -> classOf[JiraIssueSimple],
        ),
      ) {
    override protected def encodeCaseClass(writer: BsonWriter, value: JiraIssueSimpleDAO, encoderContext: EncoderContext): Unit = {
      writeFieldValue(writer, "key", value.key, encoderContext)
      writeFieldValue(writer, "createdAt", value.createdAt, encoderContext)
      writeFieldValue(writer, "data", value.data, encoderContext)
    }

    override protected def construct(fieldsData: Map[String, Any]) = {
      JiraIssueSimpleDAO(
        key = fieldsData.getOrElse("key", throw new BsonInvalidOperationException("Missing field: \"key\"")).asInstanceOf[IssueKey],
        createdAt = fieldsData
          .getOrElse("createdAt", throw new BsonInvalidOperationException("Missing field: \"createdAt\""))
          .asInstanceOf[ZonedDateTime],
        data =
          fieldsData.getOrElse("data", throw new BsonInvalidOperationException("Missing field: \"data\"")).asInstanceOf[JiraIssueSimple],
      )
    }

  }

}
