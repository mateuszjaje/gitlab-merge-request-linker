package io.gitlab.mateuszjaje
package gitlabbot.storage.mongo.codecs

import org.bson.codecs.configuration.CodecRegistries
import org.mongodb.scala.MongoClient

object ThisAppCodecRegistry:
  val customCodecs =
    CodecRegistries.fromCodecs(UTCZonedDateTimeMongoCodec, BigIntCodec, ValueClassCodec.StringUUIDCodec, ValueClassCodec.IssueKeyCodec)

  val registry = CodecRegistries.fromRegistries(
    CodecRegistries.fromRegistries(customCodecs, MongoClient.DEFAULT_CODEC_REGISTRY),
    CodecRegistries.fromProviders(BranchToKeepCodecProvider, LastCheckedDAOCodecProvider, BranchToKeepDaoCodecProvider),
    CodecRegistries.fromProviders(JiraIssueSimpleCodecProvider, JiraIssueSimpleDAOCodecProvider, ProjectNameDAOCodecProvider),
  )
