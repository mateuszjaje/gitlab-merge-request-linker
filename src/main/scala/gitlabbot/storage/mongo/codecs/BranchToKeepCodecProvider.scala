package io.gitlab.mateuszjaje
package gitlabbot.storage.mongo.codecs

import gitlabbot.shared.BranchToKeep
import gitlabbot.storage.mongo.codecs.BranchToKeepCodecProvider.BranchToKeepCodec

import org.bson.codecs.configuration.{CodecProvider, CodecRegistry}
import org.bson.codecs.{Codec, EncoderContext}
import org.bson.{BsonInvalidOperationException, BsonWriter}

object BranchToKeepCodecProvider extends CodecProvider {
  val BranchToKeepClass = classOf[BranchToKeep]

  override def get[T](clazz: Class[T], registry: CodecRegistry) =
    if (BranchToKeepClass.isAssignableFrom(clazz)) BranchToKeepCodec(registry).asInstanceOf[Codec[T]] else null

  final case class BranchToKeepCodec(reg: CodecRegistry)
      extends SemiMacroCodec[BranchToKeep](
        BranchToKeepClass,
        reg,
        Map(
          "branch"  -> classOf[String],
          "enabled" -> classOf[Boolean],
          "version" -> classOf[Int],
          "envName" -> classOf[String],
        ),
      ) {
    override protected def encodeCaseClass(writer: BsonWriter, value: BranchToKeep, encoderContext: EncoderContext): Unit = {
      writeFieldValue(writer, "branch", value.branch, encoderContext)
      writeFieldValue(writer, "enabled", value.enabled, encoderContext)
      writeFieldValue(writer, "version", value.version, encoderContext)
      writeFieldValue(writer, "envName", value.envName, encoderContext)
    }

    override protected def construct(fieldsData: Map[String, Any]) = {
      BranchToKeep(
        branch = fieldsData.getOrElse("branch", throw new BsonInvalidOperationException("Missing field: \"branch\"")).asInstanceOf[String],
        enabled =
          fieldsData.getOrElse("enabled", throw new BsonInvalidOperationException("Missing field: \"enabled\"")).asInstanceOf[Boolean],
        version = fieldsData.getOrElse("version", throw new BsonInvalidOperationException("Missing field: \"version\"")).asInstanceOf[Int],
        envName =
          fieldsData.getOrElse("envName", throw new BsonInvalidOperationException("Missing field: \"envName\"")).asInstanceOf[String],
      )
    }

  }

}
