package io.gitlab.mateuszjaje
package gitlabbot.storage.mongo.codecs

import jiraclient.models.IssueKey

import org.bson.codecs.configuration.CodecConfigurationException
import org.bson.codecs.{Codec, DecoderContext, EncoderContext}
import org.bson.{BsonReader, BsonType, BsonWriter}

import java.util.UUID
import scala.reflect.ClassTag

object ValueClassCodec:
  def string[A](decoder: String => A, encoder: A => String)(using ct: ClassTag[A]): Codec[A] = new Codec[A] {
    override def decode(reader: BsonReader, decoderContext: DecoderContext) = {
      reader.getCurrentBsonType match {
        case BsonType.STRING => decoder(reader.readString())
        case t =>
          throw new CodecConfigurationException(
            s"Could not decode ${ct.runtimeClass.getCanonicalName}, expected STRING BsonType but got '$t'.",
          )
      }
    }

    override def encode(writer: BsonWriter, value: A, encoderContext: EncoderContext): Unit =
      writer.writeString(encoder(value))

    override def getEncoderClass = ct.runtimeClass.asInstanceOf[Class[A]]
  }

  val StringUUIDCodec = string[UUID](UUID.fromString, _.toString)
  val IssueKeyCodec   = string[IssueKey](IssueKey(_), _.value)
