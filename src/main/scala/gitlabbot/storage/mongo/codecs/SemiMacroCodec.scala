package io.gitlab.mateuszjaje
package gitlabbot.storage.mongo.codecs

import org.bson.codecs.configuration.{CodecProvider, CodecRegistry}
import org.bson.codecs.{Codec, DecoderContext, Encoder, EncoderContext}
import org.bson.{BsonInvalidOperationException, BsonReader, BsonType, BsonWriter}
import org.mongodb.scala.bson.codecs.macrocodecs.MacroCodec

import scala.annotation.unused
import scala.collection.mutable

abstract class SemiMacroCodec[C](
    clazz: Class[C],
    codecRegistry: CodecRegistry,
    fields: Map[String, Class[_]],
) extends Codec[C] {
  override def getEncoderClass = clazz

  override def decode(reader: BsonReader, decoderContext: DecoderContext) = {
    val fields = decodeTOMap(reader, decoderContext)
    construct(fields)
  }

  private def decodeTOMap(reader: BsonReader, decoderContext: DecoderContext) = {
    val map = mutable.Map[String, Any]()
    reader.readStartDocument()
    while (reader.readBsonType ne BsonType.END_OF_DOCUMENT) {
      val name = reader.readName
      fields
        .get(name)
        .map(definedName => map += (name -> codecRegistry.get(definedName).decode(reader, decoderContext)))
        .getOrElse(
          reader.skipValue(),
        )
    }
    reader.readEndDocument()
    map.toMap
  }

  protected def writeFieldValue[V](writer: BsonWriter, fieldName: String, value: V, encoderContext: EncoderContext): Unit = {
    writer.writeName(fieldName)
    val codec = codecRegistry.get(value.getClass).asInstanceOf[Encoder[V]]
    encoderContext.encodeWithChildContext(codec, writer, value)
  }

  override def encode(writer: BsonWriter, value: C, encoderContext: EncoderContext): Unit = {
    writer.writeStartDocument()
    encodeCaseClass(writer, value, encoderContext)
    writer.writeEndDocument()
  }

  protected def encodeCaseClass(writer: BsonWriter, value: C, encoderContext: EncoderContext): Unit

  protected def construct(fieldsData: Map[String, Any]): C

}
