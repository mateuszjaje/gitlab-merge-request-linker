package io.gitlab.mateuszjaje
package gitlabbot.storage.mongo.codecs

import gitlabbot.shared.{BranchToKeep, JiraIssueSimple}
import gitlabbot.storage.mongo.BranchToKeepDao

import org.bson.codecs.configuration.{CodecProvider, CodecRegistry}
import org.bson.codecs.{Codec, EncoderContext}
import org.bson.{BsonInvalidOperationException, BsonWriter}

import java.time.ZonedDateTime

object BranchToKeepDaoCodecProvider extends CodecProvider {
  val BranchToKeepDaoClass = classOf[BranchToKeepDao]

  override def get[T](clazz: Class[T], registry: CodecRegistry) =
    if (BranchToKeepDaoClass.isAssignableFrom(clazz)) BranchToKeepDaoCodec(registry).asInstanceOf[Codec[T]] else null

  final case class BranchToKeepDaoCodec(reg: CodecRegistry)
      extends SemiMacroCodec[BranchToKeepDao](
        BranchToKeepDaoClass,
        reg,
        Map(
          "branch" -> classOf[BranchToKeep],
          "kind"   -> classOf[String],
        ),
      ) {
    override protected def encodeCaseClass(writer: BsonWriter, value: BranchToKeepDao, encoderContext: EncoderContext): Unit = {
      writeFieldValue(writer, "branch", value.branch, encoderContext)
      writeFieldValue(writer, "kind", value.kind, encoderContext)
    }

    override protected def construct(fieldsData: Map[String, Any]) = {
      BranchToKeepDao(
        branch =
          fieldsData.getOrElse("branch", throw new BsonInvalidOperationException("Missing field: \"branch\"")).asInstanceOf[BranchToKeep],
        kind = fieldsData.getOrElse("kind", throw new BsonInvalidOperationException("Missing field: \"kind\"")).asInstanceOf[String],
      )
    }

  }

}
