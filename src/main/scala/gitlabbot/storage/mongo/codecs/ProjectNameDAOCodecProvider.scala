package io.gitlab.mateuszjaje
package gitlabbot.storage.mongo.codecs

import gitlabbot.shared.{JiraIssueSimple, ProjectSimpleInfo}
import jiraclient.models.IssueKey

import org.bson.codecs.configuration.{CodecProvider, CodecRegistry}
import org.bson.codecs.{Codec, EncoderContext}
import org.bson.{BsonInvalidOperationException, BsonWriter}

import java.time.ZonedDateTime

object ProjectNameDAOCodecProvider extends CodecProvider:
  val ProjectNameDAOClass = classOf[ProjectSimpleInfoDAO]

  override def get[T](clazz: Class[T], registry: CodecRegistry) =
    if (ProjectNameDAOClass.isAssignableFrom(clazz)) ProjectNameDAOCodec(registry).asInstanceOf[Codec[T]] else null

  final case class ProjectNameDAOCodec(reg: CodecRegistry)
      extends SemiMacroCodec[ProjectSimpleInfoDAO](
        ProjectNameDAOClass,
        reg,
        Map(
          "id"         -> classOf[BigInt],
          "humanName"  -> classOf[String],
          "projectUrl" -> classOf[String],
          "createdAt"  -> classOf[ZonedDateTime],
        ),
      ) {
    override protected def encodeCaseClass(writer: BsonWriter, value: ProjectSimpleInfoDAO, encoderContext: EncoderContext): Unit =
      writeFieldValue(writer, "id", value.id, encoderContext)
      writeFieldValue(writer, "humanName", value.humanName, encoderContext)
      writeFieldValue(writer, "projectUrl", value.projectUrl, encoderContext)
      writeFieldValue(writer, "createdAt", value.createdAt, encoderContext)

    override protected def construct(fieldsData: Map[String, Any]) =
      ProjectSimpleInfoDAO(
        id = fieldsData.getOrElse("id", throw new BsonInvalidOperationException("Missing field: \"id\"")).asInstanceOf[BigInt],
        humanName =
          fieldsData.getOrElse("humanName", throw new BsonInvalidOperationException("Missing field: \"humanName\"")).asInstanceOf[String],
        projectUrl =
          fieldsData.getOrElse("projectUrl", throw new BsonInvalidOperationException("Missing field: \"projectUrl\"")).asInstanceOf[String],
        createdAt = fieldsData
          .getOrElse("createdAt", throw new BsonInvalidOperationException("Missing field: \"createdAt\""))
          .asInstanceOf[ZonedDateTime],
      )

  }

case class ProjectSimpleInfoDAO(
    id: BigInt,
    humanName: String,
    projectUrl: String,
    createdAt: ZonedDateTime,
):
  lazy val asPair = id -> ProjectSimpleInfo(id, humanName, projectUrl)
