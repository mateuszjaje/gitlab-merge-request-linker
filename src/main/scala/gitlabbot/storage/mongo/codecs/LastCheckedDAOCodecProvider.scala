package io.gitlab.mateuszjaje
package gitlabbot.storage.mongo.codecs

import gitlabbot.storage.mongo.LastCheckedDAO
import jiraclient.models.IssueKey

import org.bson.codecs.configuration.{CodecProvider, CodecRegistry}
import org.bson.codecs.{Codec, EncoderContext}
import org.bson.{BsonInvalidOperationException, BsonWriter}

import java.time.ZonedDateTime

object LastCheckedDAOCodecProvider extends CodecProvider {
  val LastCheckedDAOClass = classOf[LastCheckedDAO]

  override def get[T](clazz: Class[T], registry: CodecRegistry) =
    if (LastCheckedDAOClass.isAssignableFrom(clazz)) LastCheckedDAOCodec(registry).asInstanceOf[Codec[T]] else null

  final case class LastCheckedDAOCodec(reg: CodecRegistry)
      extends SemiMacroCodec[LastCheckedDAO](
        LastCheckedDAOClass,
        reg,
        Map(
          "lastChecked" -> classOf[ZonedDateTime],
          "kind"        -> classOf[String],
        ),
      ) {
    override protected def encodeCaseClass(writer: BsonWriter, value: LastCheckedDAO, encoderContext: EncoderContext): Unit = {
      writeFieldValue(writer, "lastChecked", value.lastChecked, encoderContext)
      writeFieldValue(writer, "kind", value.kind, encoderContext)
    }

    override protected def construct(fieldsData: Map[String, Any]) = {
      LastCheckedDAO(
        lastChecked = fieldsData
          .getOrElse("lastChecked", throw new BsonInvalidOperationException("Missing field: \"lastChecked\""))
          .asInstanceOf[ZonedDateTime],
        kind = fieldsData.getOrElse("kind", throw new BsonInvalidOperationException("Missing field: \"kind\"")).asInstanceOf[String],
      )
    }

  }

}
