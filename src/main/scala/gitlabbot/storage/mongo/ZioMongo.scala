package io.gitlab.mateuszjaje
package gitlabbot.storage.mongo

import gitlabbot.storage.mongo.codecs.ThisAppCodecRegistry

import org.bson.UuidRepresentation
import org.mongodb.scala.{ConnectionString, MongoClient, MongoClientSettings, WriteConcern}
import zio.{ZIO, ZLayer}

object ZioMongo:
  val client: ZLayer[ConnectionString, Nothing, MongoClient] = ZLayer.scoped {
    ZIO.fromAutoCloseable {
      for {
        uri <- ZIO.service[ConnectionString]
        clientSettings = MongoClientSettings
          .builder()
          .applyConnectionString(uri)
          .uuidRepresentation(UuidRepresentation.STANDARD)
          .codecRegistry(ThisAppCodecRegistry.registry)
          .writeConcern(WriteConcern.ACKNOWLEDGED)
          .build()
      } yield MongoClient(clientSettings)
    }
  }
