package io.gitlab.mateuszjaje
package gitlabbot.storage.mongo

import org.mongodb.scala.SingleObservable
import zio.{IO, ZIO}

import scala.concurrent.{Future, Promise}

trait MongoCompat:
  extension [T](value: SingleObservable[T]) {
    def toZio: IO[Throwable, T] =
      val prom = Promise[Either[Throwable, T]]()
      value.subscribe(x => prom.success(Right(x)), x => prom.success(Left(x)))
      ZIO.fromFuture(_ => prom.future).orDie.absolve

  }

  extension [T](value: Future[T]) {
    def toZio: IO[Throwable, T] =
      ZIO.fromFuture(_ => value)

  }
