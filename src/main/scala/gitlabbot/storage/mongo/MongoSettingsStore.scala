package io.gitlab.mateuszjaje
package gitlabbot.storage.mongo

import gitlabbot.shared.MyZIOSyntax.catchNonFatal
import gitlabbot.shared.{BranchToKeep, MgthError}
import gitlabbot.storage.SettingsStore
import gitlabbot.storage.mongo.MongoCompat
import gitlabbot.{MongoDatabaseName, ThisAppMongoConfig}

import org.mongodb.scala.model.{Filters, ReplaceOptions}
import org.mongodb.scala.{MongoClient, MongoCollection, ObservableImplicits}
import zio.{Task, ZIO, ZLayer}

import java.time.ZonedDateTime
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.Try

case class LastCheckedDAO(lastChecked: ZonedDateTime, kind: String = "last_checked_mrs")

case class BranchToKeepDao(branch: BranchToKeep, kind: String = "branch_to_keep")

object MongoSettingsStore:
  val live: ZLayer[MongoClient & MongoDatabaseName, MgthError, MongoSettingsStore] =
    ZLayer
      .fromZIO(for {
        client <- ZIO.service[MongoClient]
        cfg    <- ZIO.service[MongoDatabaseName]
        db     <- ZIO.catchNonFatal(client.getDatabase(cfg.name))
        col1   = db.getCollection[LastCheckedDAO]("Settings")
        col2   = db.getCollection[BranchToKeepDao]("Settings")
        result = new MongoSettingsStore(col1, col2)
      } yield result)
      .mapError(MgthError.unknown("Cant construct MongoSettingsStore"))

class MongoSettingsStore(
    lastCheckedCol: MongoCollection[LastCheckedDAO],
    branchesToKeepCol: MongoCollection[BranchToKeepDao],
) extends SettingsStore
    with MongoCompat:

  override def storeLastUpdated(date: ZonedDateTime) =
    lastCheckedCol.replaceOne(Filters.eq("kind", "last_checked_mrs"), LastCheckedDAO(date), ReplaceOptions().upsert(true)).toZio.unit

  override def loadLastChecked =
    lastCheckedCol
      .find(Filters.eq("kind", "last_checked_mrs"))
      .headOption()
      .toZio
      .map(_.map(_.lastChecked))

  override def readBranchesToKeep =
    branchesToKeepCol
      .find(Filters.eq("kind", "branch_to_keep"))
      .collect()
      .toZio
      .map(_.toList.map(_.branch))
