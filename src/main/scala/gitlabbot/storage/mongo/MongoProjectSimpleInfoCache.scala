package io.gitlab.mateuszjaje
package gitlabbot.storage.mongo

import gitlabbot.shared.MyZIOSyntax.catchNonFatal
import gitlabbot.shared.{JiraIssueSimple, MgthError, ProjectSimpleInfo}
import gitlabbot.storage.mongo.codecs.ProjectSimpleInfoDAO
import gitlabbot.storage.{JiraSimpleIssueCache, ProjectSimpleInfoCache}
import gitlabbot.{MongoDatabaseName, ThisAppMongoConfig}
import gitlabclient.GitlabRestAPIV2
import jiraclient.JiraRestAPIV2
import jiraclient.models.IssueKey

import com.typesafe.scalalogging.LazyLogging
import org.bson.codecs.*
import org.bson.codecs.configuration.{CodecConfigurationException, CodecProvider, CodecRegistries}
import org.bson.{BsonReader, BsonType, BsonWriter, UuidRepresentation}
import org.mongodb.scala.*
import org.mongodb.scala.bson.codecs.Macros
import org.mongodb.scala.model.Filters.equal
import org.mongodb.scala.model.{Filters, IndexOptions}

import java.time.{Instant, ZoneOffset, ZonedDateTime}
import java.util.UUID
import java.util.concurrent.TimeUnit
import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.reflect.ClassTag
import org.mongodb.scala
import org.mongodb.scala.result.InsertOneResult
import zio.{durationInt, IO, Schedule, Task, UIO, ZIO, ZLayer}

object MongoProjectSimpleInfoCache:
  val live: ZLayer[MongoClient & MongoDatabaseName, MgthError, ProjectSimpleInfoCache] = ZLayer
    .fromZIO(for {
      mongoclient <- ZIO.service[MongoClient]
      cfg         <- ZIO.service[MongoDatabaseName]
      database    <- ZIO.catchNonFatal(mongoclient.getDatabase(cfg.name))
      col    = database.getCollection[ProjectSimpleInfoDAO]("ProjectInfo")
      result = new MongoProjectSimpleInfoCache(col)
      _ <- result.initialize()
    } yield result)
    .mapError(MgthError.unknown("Cant construct MongoProjectNameCache"))

class MongoProjectSimpleInfoCache(col: MongoCollection[ProjectSimpleInfoDAO])
    extends ProjectSimpleInfoCache
    with LazyLogging
    with MongoCompat:

  def initialize(): Task[Unit] =
    val options = IndexOptions().expireAfter(1, TimeUnit.DAYS)

    col.createIndex(Document("id" -> 1)).toZio *>
      col.createIndex(Document("createdAt" -> 1), options).toZio.unit

  override def store(projectId: BigInt, projectName: String, projectUrl: String): IO[MgthError, Unit] =
    col
      .insertOne(ProjectSimpleInfoDAO(projectId, projectName, projectUrl, ZonedDateTime.now()))
      .toZio
      .mapError(MgthError.databaseFail(s"MongoProjectSimpleInfoCache.store($projectId,$projectName,$projectUrl)"))
      .unit

  override def getProjectSimpleInfo(ids: Vector[BigInt]): IO[MgthError, Map[BigInt, ProjectSimpleInfo]] =
    col
      .find(Filters.in("id", ids*))
      .collect()
      .toZio
      .mapBoth(MgthError.databaseFail(s"MongoProjectSimpleInfoCache.getProjectSimpleInfo($ids)"), _.map(_.asPair).toMap)
