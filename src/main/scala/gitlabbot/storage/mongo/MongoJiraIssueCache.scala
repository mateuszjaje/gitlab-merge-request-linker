package io.gitlab.mateuszjaje
package gitlabbot.storage.mongo

import gitlabbot.shared.MyZIOSyntax.catchNonFatal
import gitlabbot.shared.{JiraIssueSimple, MgthError}
import gitlabbot.storage.JiraSimpleIssueCache
import gitlabbot.{JiraEpicCustomColumnNumber, MongoDatabaseName}
import jiraclient.JiraRestAPIV2
import jiraclient.models.IssueKey

import com.typesafe.scalalogging.LazyLogging
import org.mongodb.scala.*
import org.mongodb.scala.model.Filters.equal
import org.mongodb.scala.model.IndexOptions

import java.time.ZonedDateTime
import java.util.concurrent.TimeUnit
import scala.reflect.ClassTag
import org.mongodb.scala
import org.mongodb.scala.result.InsertOneResult
import zio.{IO, Task, UIO, ZIO, ZLayer}

case class JiraIssueSimpleDAO(
    key: IssueKey,
    createdAt: ZonedDateTime,
    data: JiraIssueSimple,
)

object MongoJiraIssueCache:
  val live: ZLayer[MongoClient & MongoDatabaseName & JiraEpicCustomColumnNumber, MgthError, MongoJiraIssueCache] = ZLayer
    .fromZIO(for {
      mongoclient <- ZIO.service[MongoClient]
      cfg         <- ZIO.service[MongoDatabaseName]
      epicValue   <- ZIO.service[JiraEpicCustomColumnNumber]
      database    <- ZIO.catchNonFatal(mongoclient.getDatabase(cfg.name))
      col    = database.getCollection[JiraIssueSimpleDAO]("JiraIssueCache")
      result = new MongoJiraIssueCache(epicValue.no.map(_.toString), col)
      _ <- result.initialize()
    } yield result)
    .mapError(MgthError.unknown("Cant construct MongoJiraIssueCache"))

class MongoJiraIssueCache(epicCustomColumnNo: Option[String], col: MongoCollection[JiraIssueSimpleDAO])
    extends JiraSimpleIssueCache
    with LazyLogging
    with MongoCompat {

  def initialize(): Task[Unit] = {
    val uniqueOpts = IndexOptions().unique(true)
    val ttlOpts    = IndexOptions().expireAfter(1, TimeUnit.DAYS)

    for {
      _ <- col.createIndex(Document("key" -> 1), uniqueOpts).toZio
      _ <- col.createIndex(Document("createdAt" -> 1), ttlOpts).toZio
    } yield ()
  }

  def store(data: JiraIssueSimpleDAO): IO[MgthError, InsertOneResult] =
    col
      .insertOne(data)
      .toZio
      .mapError(MgthError.databaseFail(s"MongoJiraIssueCache.store($data)"))

  def read(key: IssueKey): IO[MgthError, Option[JiraIssueSimpleDAO]] = {
    col
      .find(equal("key", key))
      .collect()
      .toZio
      .mapError(MgthError.databaseFail(s"MongoJiraIssueCache.read($key)"))
      .flatMap { x =>
        ZIO.fromEither(x.toList match {
          case Nil         => Right(None)
          case elem :: Nil => Right(Some(elem))
          case more        => Left(MgthError.invalidState(s"Dafuq more results returned $more"))
        })
      }
  }

  override def getIssueSimple(jira: JiraRestAPIV2[UIO], key: IssueKey) = {
    read(key)
      .flatMap {
        case None =>
          logger.info(s"Creating jira issue cache for $key")
          jira
            .getIssue(key)
            .absolve
            .mapError(MgthError(_))
            .flatMap { ticket =>
              // if epic value no is not defined, skip epic handle
              val customFieldEpic = epicCustomColumnNo
                .flatMap(no => ticket.fields.customFields.get(no))
                .flatMap(_.asString)
                .map(IssueKey.apply)
              val parentEpic = ticket.fields.parent.map(_.key)
              val epicMaybe  = customFieldEpic.orElse(parentEpic)

              epicMaybe
                .map(getIssueSimple(jira, _).map(Some(_)).map(_ -> ticket))
                .getOrElse(ZIO.succeed(None -> ticket))
            }
            .map { case (epic, ticket) =>
              JiraIssueSimple(ticket.key, ticket.htmlSelf, ticket.fields.summary, epic.map(_.key), ticket.fields.description.getOrElse(""))
            }
            .map(JiraIssueSimpleDAO(key, ZonedDateTime.now(), _))
            .flatMap(store)
            .zipRight(read(key))
            .flatMap {
              case Some(elem) => ZIO.succeed(elem.data)
              case None       => ZIO.fail(MgthError.invalidState(s"entry in database missing still $key"))
            }
        case Some(x) =>
          ZIO.succeed(x.data)
      }
  }

  override def getCachedIssues(): IO[MgthError, List[(ZonedDateTime, IssueKey)]] = {
    col
      .find()
      .collect()
      .toZio
      .map(_.toList.map(x => x.createdAt -> x.key))
      .mapError(MgthError.databaseFail("MongoJiraIssueCache.getCachedIssues()"))
  }

}
