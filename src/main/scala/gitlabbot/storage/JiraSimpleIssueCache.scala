package io.gitlab.mateuszjaje
package gitlabbot.storage

import gitlabbot.shared.{JiraIssueSimple, MgthError}
import jiraclient.JiraRestAPIV2
import jiraclient.models.IssueKey

import zio.{IO, UIO}

import java.time.ZonedDateTime

trait JiraSimpleIssueCache:
  def getIssueSimple(jira: JiraRestAPIV2[UIO], key: IssueKey): IO[MgthError, JiraIssueSimple]
  def getCachedIssues(): IO[MgthError, List[(ZonedDateTime, IssueKey)]]
