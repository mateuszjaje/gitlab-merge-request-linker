package io.gitlab.mateuszjaje
package gitlabbot

import gitlabbot.CommonLayers.*
import gitlabbot.logic.UpdateLinksInMRsWorker
import gitlabbot.shared.MgthError
import gitlabbot.storage.SettingsStore
import gitlabbot.storage.mongo.{MongoJiraIssueCache, MongoProjectSimpleInfoCache, MongoSettingsStore, ZioMongo}
import gitlabclient.sttpbackend.ZSttpGitlabAPI
import gitlabclient.{GitlabConfig, GitlabRestAPIConfig, GitlabRestAPIV2}
import jiraclient.models.IssueKey
import jiraclient.sttpbackend.zio.ZSttpJiraAPI
import jiraclient.{JiraConfig, JiraRestAPIConfig, JiraRestAPIV2}

import com.mongodb.ConnectionString
import org.mongodb.scala.MongoClient
import sttp.client3.httpclient.zio.HttpClientZioBackend
import zio.Console.printLine
import zio.Schedule.WithState
import zio.logging.backend.SLF4J
import zio.{durationInt, Clock, Exit, IO, Runtime, Schedule, UIO, URIO, Unsafe, ZIO, ZLayer}

import java.time.{OffsetDateTime, ZonedDateTime}

val linkAllOpenedAndRecentlyMergedMrsProgram: ZIO[SettingsStore with UpdateLinksInMRsWorker, MgthError, Unit] = for {
  w             <- ZIO.service[UpdateLinksInMRsWorker]
  settingsStore <- ZIO.service[SettingsStore]
  currentTime   <- Clock.currentDateTime
  lastUpdate <- settingsStore.loadLastChecked
    .map(_.getOrElse(ZonedDateTime.now().minusDays(7)))
    .mapError(MgthError.databaseFail("lastCheckedRead"))
  _      <- printLine(s"Last update is $lastUpdate").mapError(MgthError.unknown("printout error"))
  result <- w.linkAllOpenedAndRecentlyMergedMrs(Some(lastUpdate))
  _      <- Printouts.printOutUpdatedMrs(result).mapError(MgthError.unknown("printout error"))
  _      <- settingsStore.storeLastUpdated(currentTime.toZonedDateTime).mapError(MgthError.databaseFail("storeLastUpdated "))
  _      <- printLine("UpdateLinksInMRsWorker.linkAllOpenedAndRecentlyMergedMrs done.").mapError(MgthError.unknown("printout error"))
} yield ()

val slf4jLogger = Runtime.removeDefaultLoggers >>> SLF4J.slf4j

val action: ZIO[Any, MgthError, Unit] = linkAllOpenedAndRecentlyMergedMrsProgram
  .provide(
    UpdateLinksInMRsWorker.live,
    ZSttpGitlabAPI.GitlabApiLayer,
    ZSttpJiraAPI.JiraApiLayer,
    MongoSettingsStore.live,
    MongoJiraIssueCache.live,
    MongoProjectSimpleInfoCache.live,
    ZioMongo.client,
    mongoDatabaseName,
    epicColumnNameNumber,
    monitoredGroups,
    linkingConfig,
    jiraConfig,
    gitlabConfig,
    appConfig,
    connectionString,
    backend,
  )
  .provide(slf4jLogger)

@main
def main(args: String*): Unit = {
  val schedule = CliArguments
    .parse(args)
    .map(x => Schedule.fixed(zio.Duration.fromScala(x.interval)).upTo(zio.Duration.fromScala(x.duration)))

  val work = schedule.flatMap(action.repeat(_).unit)
  Unsafe.unsafe(implicit unsafe => println(Runtime.default.unsafe.run(work)))
}
