package io.gitlab.mateuszjaje
package gitlabbot.shared

import gitlabclient.models.{GitlabBranchInfo, MergeRequestInfo, MergeRequestState, MergeRequestStates}
import jiraclient.models.IssueKey

case class JiraIssueSimple(
    key: IssueKey,
    webUrl: String,
    title: String,
    epicKey: Option[IssueKey],
    description: String,
) {
  lazy val isIgnoreMe = webUrl == "ignore_me"
}

object JiraIssueSimple {
  def IgnoreMe(key: IssueKey) = JiraIssueSimple(key, "ignore_me", "ignore_me", None, "ignore_me")
}

case class ProjectSimpleInfo(
    id: BigInt,
    humanName: String,
    projectUrl: String,
)

case class BranchToKeep(branch: String, enabled: Boolean, version: Int, envName: String) {
  lazy val fullEnvironmentName = s"Temp $envName"
}

case class BranchInfo(
    projectId: BigInt,
    projectName: String,
    name: String,
    webUrl: String,
    _branch: GitlabBranchInfo,
)

object BranchInfo:
  def apply(projId: ProjectSimpleInfo, branch: GitlabBranchInfo): BranchInfo = {
    new BranchInfo(
      projId.id,
      projId.humanName,
      branch.name,
      s"${projId.projectUrl}/tree/${branch.name}",
      branch,
    )
  }

case class MrInfo(
    projectId: BigInt,
    projectName: String,
    uniqueId: BigInt,
    id: BigInt,
    title: String,
    description: String,
    state: MergeRequestState,
    webUrl: String,
    open: Boolean,
    _mr: MergeRequestInfo,
)

object MrInfo:
  def apply(mr: MergeRequestInfo, projectNames: Map[BigInt, ProjectSimpleInfo]): MrInfo = {
    new MrInfo(
      mr.target_project_id,
      projectNames(mr.target_project_id).humanName,
      mr.id,
      mr.iid,
      mr.title,
      mr.description.getOrElse(""),
      mr.state,
      mr.web_url,
      mr.state == MergeRequestStates.Open,
      mr,
    )
  }
