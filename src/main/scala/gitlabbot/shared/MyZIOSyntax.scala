package io.gitlab.mateuszjaje
package gitlabbot.shared

import zio.{IO, ZIO}

import scala.util.Try

object MyZIOSyntax:
  class ZIORightPartial[L]:
    def apply[B](value: B): IO[L, B] = ZIO.fromEither(Right(value))

  extension (e: ZIO.type) {
    def catchNonFatal[A](action: => A) = e.fromTry(Try(action))
    def rightE[A]                      = new ZIORightPartial[A]

  }
