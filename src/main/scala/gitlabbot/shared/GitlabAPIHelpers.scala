package io.gitlab.mateuszjaje
package gitlabbot.shared

import gitlabbot.MonitoredProjects
import gitlabbot.shared.{BranchInfo, MgthError, ProjectSimpleInfo}
import gitlabbot.storage.ProjectSimpleInfoCache
import gitlabclient.*
import gitlabclient.models.{GitlabBranchInfo, MergeRequestInfo, MergeRequestState, MergeRequestStates}
import jiraclient.models.IssueKey

import com.typesafe.scalalogging.LazyLogging
import sttp.client3.SttpClientException.ReadException
import zio.{IO, UIO, ZIO}

import java.time.ZonedDateTime

trait GitlabAPIHelpers extends LazyLogging {
  def gitlab: GitlabRestAPIV2[UIO]

  def projectNameCache: ProjectSimpleInfoCache

  def fetchProject(projectId: BigInt) =
    retried(5, () => gitlab.getProject(projectId).absolve)

  def getUserEvents(user: BigInt, since: UtcDate) =
    retried(5, () => gitlab.getUserContributionEvents(userId = user, since = since).absolve)

  protected def findAllMRsWithIssueKeyInName(
      monitoredProjects: MonitoredProjects,
      issue: IssueKey,
  ): IO[MgthError, Vector[MergeRequestInfo]] = {
    (for {
      groupMrs <- ZIO
        .foreach(monitoredProjects.groups)(groupId => gitlab.getGroupMergeRequests(groupId, search = issue.value).absolve)
        .map(_.flatten)
      extraProjectsMrs <- ZIO
        .foreach(monitoredProjects.extraProjects)(projectI => gitlab.getMergeRequests(projectI, search = issue.value).absolve)
        .map(_.flatten)
    } yield groupMrs ++ extraProjectsMrs)
      .map { results =>
        val (matching, other) = results.partition(mr => IssueFinder.findIssueIn(mr).contains(issue))
        if (other.nonEmpty) {
          logger.error(
            s"Gitlab requested for mrs for $issue returned strange results: ${other.map(_.copy(description = Some(""))).mkString("\n\t", "\n\t", "")}",
          )
        }
        matching
      }
      .mapError(MgthError(_))
  }

  private def _resolveProjectNames(projectIds: Iterable[BigInt]): IO[MgthError, Map[BigInt, ProjectSimpleInfo]] = {
    for {
      knownProjects <- projectNameCache.getProjectSimpleInfo(projectIds.toVector)
      unknownProjectIds = projectIds.toSet.diff(knownProjects.keys.toSet)
      unknownProjects <- ZIO
        .foreach(unknownProjectIds) { prID =>
          logger.info(s"Creating project info cache for $prID")
          fetchProject(prID).flatMap { projectInfo =>
            projectNameCache
              .store(projectInfo.id, projectInfo.name, projectInfo.web_url)
              .as(projectInfo.id -> ProjectSimpleInfo(projectInfo.id, projectInfo.name, projectInfo.web_url))
          }
        }
        .map(_.toMap)
    } yield unknownProjects ++ knownProjects
  }

  protected def resolveProjectNames(mrs: Seq[MergeRequestInfo]): IO[MgthError, Map[BigInt, ProjectSimpleInfo]] =
    _resolveProjectNames(mrs.map(_.target_project_id))

  protected def resolveProjectNames(mrs: Iterable[Iterable[MergeRequestInfo]]): IO[MgthError, Map[BigInt, ProjectSimpleInfo]] =
    _resolveProjectNames(mrs.flatMap(_.map(_.target_project_id)))

  protected def loadAllBranchesInKnownProjects(
      supportedProjects: Set[BigInt],
      issueKey: Option[String] = None,
  ): IO[MgthError, Vector[BranchInfo]] = {
    def find(repoId: BigInt): IO[MgthError, Vector[BranchInfo]] = {
      for {
        branches <- gitlab.getBranches(repoId, issueKey).absolve.mapError(MgthError(_))
        project  <- _resolveProjectNames(Seq(repoId)).map(_.values.head)
        result = branches.map(BranchInfo(project, _))
      } yield result
    }

    ZIO.foreach(supportedProjects.toVector)(find).map(_.flatten)
  }

  protected def findAllOpenMrs(group: EntityId, updatedAfter: Option[ZonedDateTime]): IO[MgthError, Vector[MergeRequestInfo]] =
    findMrsByState(group, MergeRequestStates.Open, updatedAfter)

  protected def findAllMergedMrs(group: String, updatedAfter: Option[ZonedDateTime]): IO[MgthError, Vector[MergeRequestInfo]] =
    findMrsByState(group, MergeRequestStates.Merged, updatedAfter)

  private def findMrsByState(group: EntityId, state: MergeRequestState, updatedAfter: Option[ZonedDateTime]) =
    gitlab.getGroupMergeRequests(group, updatedAfter = updatedAfter, state = state).absolve.mapError(MgthError(_))

  def findIssuesInOpenMRs(group: String, updatedAfter: Option[ZonedDateTime]): IO[MgthError, Set[IssueKey]] =
    findAllOpenMrs(group, updatedAfter).map(_.flatMap(IssueFinder.findIssueIn(_).toList).toSet)

  def findIssuesInRecentlyMergedMrs(group: String, since: ZonedDateTime): IO[MgthError, Set[IssueKey]] =
    findMrsByState(group, MergeRequestStates.Merged, Some(since))
      .map(_.flatMap(IssueFinder.findIssueIn(_).toList).toSet)

  def checkCommitRefs(commit: String, repo: BigInt) =
    gitlab
      .getCommitRefs(repo, commit)
      .absolve
      .mapError(MgthError(_))

  protected def retried[T](retiesNumber: Int, task: () => IO[GitlabError, T]): IO[MgthError, T] = {
    def action(retries: Int): IO[GitlabError, T] = {
      task()
        .catchSome {
          case GitlabRequestingError(_, _, _: ReadException) if retries > 0 => action(retries - 1)
          case GitlabHttpError(500, _, _, _, _) if retries > 0              => action(retries - 1)
        }
    }

    action(retiesNumber).mapError(MgthError(_))
  }

}
