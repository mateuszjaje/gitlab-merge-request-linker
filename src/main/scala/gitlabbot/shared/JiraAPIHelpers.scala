package io.gitlab.mateuszjaje
package gitlabbot.shared

import jiraclient.models.{Icon, IssueKey, JiraRemoteLink, RemoteIssueLinkIdentifies}
import jiraclient.{JiraError, JiraRestAPIV2}

import com.typesafe.scalalogging.LazyLogging
import zio.{IO, UIO}

import java.net.{ConnectException, SocketTimeoutException}

trait JiraAPIHelpers extends LazyLogging {
  def jira: JiraRestAPIV2[UIO]

  protected def deleteJiraRemoteLink(ticket: IssueKey, remoteLinkId: Int): IO[MgthError, Unit] = {
    logger.info(s"Deleting issue link $remoteLinkId from $ticket")
    retried(5, () => jira.deleteRemoteLinkById(ticket, remoteLinkId).absolve)
  }

  def createOrUpdateIssueLink(
      issue: IssueKey,
      globalId: String,
      link: String,
      linkName: String,
      resolved: Boolean,
      icon: Icon,
  ): IO[MgthError, RemoteIssueLinkIdentifies] =
    retried(5, () => jira.createOrUpdateIssueLink(issue, globalId, link, linkName, resolved, Some(icon)).absolve)

  protected def getIssueLinks(issue: IssueKey): IO[MgthError, Vector[JiraRemoteLink]] =
    retried(5, () => jira.getIssueRemoteLinks(issue).absolve)

  private def retried[T](retiesNumber: Int, task: () => IO[JiraError, T]): IO[MgthError, T] = {
    def action(retries: Int): IO[JiraError, T] = {
      task()
        .catchSome {
          case jiraclient.RequestingError(_, _, _: SocketTimeoutException) if retries > 0 => action(retries - 1)
          case jiraclient.RequestingError(_, _, _: ConnectException) if retries > 0       => action(retries - 1)
        }
    }

    action(retiesNumber).mapError(MgthError(_))
  }

}
