package io.gitlab.mateuszjaje
package gitlabbot.shared

import gitlabclient.GitlabError
import jiraclient.JiraError

import pureconfig.error.ConfigReaderFailures

import java.io.{PrintWriter, StringWriter}

object CollectStackTrace:
  extension (t: Throwable) {
    def collectStackTrace: String =
      val sw = new StringWriter
      t.printStackTrace(new PrintWriter(sw))
      sw.toString

  }

import gitlabbot.shared.CollectStackTrace.*

trait MgthError

object MgthError {
  def apply(cause: GitlabError): MgthError = CausedByGitlab("", cause, new RuntimeException("show me the path!"))

  def apply(cause: JiraError): MgthError = CausedByJira("", cause, new RuntimeException("show me the path!"))

  def apply(cause: Throwable): MgthError = CausedByThrowable("", cause)

  def jira(desc: String)(cause: JiraError): MgthError = CausedByJira(desc, cause, new RuntimeException("show me the path!"))

  def invalidState(desc: String): MgthError = InvalidState(desc)

  def invalidData(desc: String): MgthError = InvalidData(desc)

  def unknown(desc: String)(cause: Throwable): MgthError = CausedByThrowable(desc, cause)

  def databaseFail(desc: String)(cause: Throwable): MgthError = CausedByDatabase(desc, cause)

  def configRead(desc: String)(cause: ConfigReaderFailures): MgthError = CausedByPureconfig(desc, cause)
}

case class CausedByGitlab(desc: String, cause: GitlabError, place: Throwable) extends MgthError {
  override def toString = s"CausedByGitlab($desc, $cause) path:\n${place.collectStackTrace}"
}

case class CausedByJira(desc: String, cause: JiraError, place: Throwable) extends MgthError {
  override def toString = s"CausedByJira($desc, $cause) path:\n${place.collectStackTrace}"
}

case class CausedByDomainError(desc: String, cause: Throwable) extends MgthError

case class CausedByThrowable(desc: String, cause: Throwable) extends MgthError

case class CausedByDatabase(desc: String, cause: Throwable) extends MgthError

case class CausedByPureconfig(desc: String, cause: ConfigReaderFailures) extends MgthError

case class InvalidState(desc: String) extends MgthError

case class InvalidData(desc: String) extends MgthError
