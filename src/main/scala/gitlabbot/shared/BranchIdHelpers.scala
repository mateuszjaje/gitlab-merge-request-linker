package io.gitlab.mateuszjaje
package gitlabbot.shared

import gitlabclient.models.MergeRequestInfo

trait BranchIdHelpers {

  def findFreeBranches(mrs: Vector[MergeRequestInfo], branches: Vector[BranchInfo]): Vector[BranchInfo] = {
    val ids = mrs.map(branchUniqueId).toSet
    branches.filterNot(branch => ids.contains(branchUniqueId(branch)))
  }

  protected def branchUniqueId(branch: BranchInfo) =
    s"b:${branch.name}:${branch.projectId}"

  protected def branchUniqueId(mr: MergeRequestInfo) =
    s"b:${mr.source_branch}:${mr.target_project_id}"

  protected def mergeRequestUniqueId(mr: MergeRequestInfo): String =
    s"mr:${mr.id}:${mr.target_project_id}"

}
