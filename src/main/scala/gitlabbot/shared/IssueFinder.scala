package io.gitlab.mateuszjaje
package gitlabbot.shared

import gitlabclient.models.MergeRequestInfo
import jiraclient.models.IssueKey

object IssueFinder:
  def findIssueIn(mr: MergeRequestInfo): Option[IssueKey] = findIssueIn(mr.source_branch) orElse findIssueIn(mr.title)

  def findIssueIn(data: String): Option[IssueKey] =
    """(?i)([A-Z]+)-\d{1,7}""".r.findFirstIn(data).map(_.toUpperCase).map(IssueKey(_))
