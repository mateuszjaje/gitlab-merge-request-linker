package io.gitlab.mateuszjaje
package gitlabbot

import gitlabbot.logic.GitlabMRDescriptionRenderingConfig
import gitlabbot.shared.MgthError
import gitlabclient.GitlabConfig
import jiraclient.{AuthMechanism, JiraConfig}

import pureconfig.*
import pureconfig.generic.derivation.default.*

import scala.math

case class MongoDatabaseName(name: String)

object MongoDatabaseName:
  implicit val MongoDatabaseSettingConfigReader: ConfigReader[MongoDatabaseName] =
    ConfigReader[String].map(MongoDatabaseName.apply)

case class MonitoredProjects(groups: Vector[BigInt], extraProjects: Vector[BigInt]) derives ConfigReader

case class ThisAppMongoConfig(
    uri: String,
    database: MongoDatabaseName,
) derives ConfigReader

case class ImageLinksConfig(
    epicLink: Option[String],
    ticketLink: Option[String],
) derives ConfigReader:
  val epicImageMd: Option[String]   = epicLink.map(x => s"![epic]($x)")
  val ticketImageMd: Option[String] = ticketLink.map(x => s"![ticket]($x)")

implicit val jiraAuthMechanismConfigReader: ConfigReader[AuthMechanism] =
  ConfigReader.configConfigReader.map(jiraclient.AuthMechanism.fromConfig)

case class JiraEpicCustomColumnNumber(no: Option[Int])

object JiraEpicCustomColumnNumber:
  implicit val JiraEpicCustomColumnNumberConfigReader: ConfigReader[JiraEpicCustomColumnNumber] =
    ConfigReader[Option[Int]].map(JiraEpicCustomColumnNumber(_))

case class ThisAppJiraConfig(
    address: String,
    auth: AuthMechanism,
    epicCustomColumnNumber: JiraEpicCustomColumnNumber,
) derives ConfigReader:
  val toBareJiraConfig = jiraclient.JiraConfig(address, auth)

implicit val gitlabConfigReader: ConfigReader[GitlabConfig] = ConfigReader.configConfigReader.map(gitlabclient.GitlabConfig.fromConfig)

case class ThisAppConfig(
    mongo: ThisAppMongoConfig,
    gitlab: GitlabConfig,
    jira: ThisAppJiraConfig,
    monitoredProjects: MonitoredProjects,
    includedProjects: Set[BigInt],
    imageLinks: ImageLinksConfig,
) derives ConfigReader

def loadThisAppConfiguration =
  zio.ZIO
    .fromEither(ConfigSource.default.load[ThisAppConfig])
    .mapError(MgthError.configRead("Cant load configuration"))
