package io.gitlab.mateuszjaje
package gitlabbot

import gitlabbot.loadThisAppConfiguration
import gitlabbot.logic.{GitlabLinkingMRsConfig, GitlabMRDescriptionRenderingConfig}
import gitlabbot.shared.MgthError
import gitlabclient.GitlabConfig
import jiraclient.JiraConfig

import com.mongodb.ConnectionString
import sttp.client3.*
import sttp.client3.httpclient.zio.HttpClientZioBackend
import zio.*

object CommonLayers:
  val backend: ZLayer[Any, MgthError, SttpBackend[Task, Any]] =
    ZLayer.scoped {
      HttpClientZioBackend
        .scoped()
        .mapError(MgthError.unknown("Cant construct HttpClientZioBackend")(_))
        .map(backend => backend: SttpBackend[Task, Any])
    }

  val appConfig: Layer[MgthError, ThisAppConfig] = ZLayer.fromZIO(loadThisAppConfiguration)
  val gitlabConfig                               = ZLayer.fromFunction((cfg: ThisAppConfig) => cfg.gitlab)
  val jiraConfig                                 = ZLayer.fromFunction((cfg: ThisAppConfig) => cfg.jira.toBareJiraConfig)
  val epicColumnNameNumber                       = ZLayer.fromFunction((cfg: ThisAppConfig) => cfg.jira.epicCustomColumnNumber)
  val connectionString                           = ZLayer.fromFunction((cfg: ThisAppConfig) => new ConnectionString(cfg.mongo.uri))
  val mongoDatabaseName                          = ZLayer.fromFunction((cfg: ThisAppConfig) => cfg.mongo.database)
  val monitoredGroups                            = ZLayer.fromFunction((cfg: ThisAppConfig) => cfg.monitoredProjects)
  val linkingConfig = ZLayer.fromFunction { (cfg: ThisAppConfig) =>
    GitlabLinkingMRsConfig(
      GitlabMRDescriptionRenderingConfig(cfg.imageLinks.epicImageMd, cfg.imageLinks.ticketImageMd, "=============================="),
      cfg.includedProjects,
    )
  }
